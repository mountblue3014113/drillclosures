function limitFunctionCallCount(cb, n) {
    if (typeof cb !== 'function') {
        throw new Error('First argument must be a function');
    }
    if (typeof n !== 'number' || n < 0) {
        throw new Error('Second argument must be a non-negative number');
    }
    let callCount = 0;
    return function(...args){
        if(callCount>=n){
            throw new Error('error');
        }
        callCount++;
        return cb(...args);
    }
}

module.exports = limitFunctionCallCount;
