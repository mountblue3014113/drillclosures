function cacheFunction(cb) {
    if (typeof cb !== 'function') {
        throw new Error('Argument must be a function');
    }

    const cache = {};

    return function (...args) {
        const key = JSON.stringify(args); 
        if (cache[key] !== undefined) {
            console.log('Result received from cache');
            return cache[key]; 
        }
        const result = cb(...args);
        cache[key] = result;
        return result;
    };
}

module.exports = cacheFunction;
