const counterFactory = require("../counterFactory.cjs");

let counter = counterFactory();

try {
  console.log(counter.increment());
  console.log(counter.increment());
  console.log(counter.decrement());
  console.log(counter.decrement());
  console.log(counter.decrement());
} catch (error) {
  console.error(error.message);
}
