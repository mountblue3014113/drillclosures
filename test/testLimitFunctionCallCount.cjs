const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function sampleFunction() {
    console.log('Sample function called');
}

const limitedSampleFunction = limitFunctionCallCount(sampleFunction, 3);

try{
    limitedSampleFunction();
limitedSampleFunction();
limitedSampleFunction();
limitedSampleFunction(); 
}catch(error){
    console.error(error.message);
}