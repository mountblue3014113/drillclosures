const cacheFunction = require('../cacheFunction.cjs');

function sampleFunction(x, y) {
    console.log('Sample function called');
    return x + y;
}

const cachedSampleFunction = cacheFunction(sampleFunction);

console.log(cachedSampleFunction(2, 3));
console.log(cachedSampleFunction(2, 3)); 
console.log(cachedSampleFunction(4, 5));
console.log(cachedSampleFunction(4, 5));
