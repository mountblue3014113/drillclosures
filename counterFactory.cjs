function counterFactory() {
  let count = 0;

  const increment = () => {
    count++;
    return count;
  };

  const decrement = () => {
    if (count > 0) {
      count--;
    } else {
      throw new Error("Counter cannot be decremented below zero");
    }
    return count;
  };

  return { increment, decrement };
}

module.exports = counterFactory;
